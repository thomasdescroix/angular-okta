import { TestBed } from '@angular/core/testing';
import { OktaAuthService } from '@okta/okta-angular';

import { AuthInterceptor } from './auth.interceptor';

describe('AuthInterceptor', () => {
  let interceptor: AuthInterceptor;

  beforeEach(() => {
    const oktaAuthServiceStub = () => ({ getAccessToken: () => ({}) });
    TestBed.configureTestingModule({
      providers: [
        AuthInterceptor,
        { provide: OktaAuthService, useFactory: oktaAuthServiceStub }
      ]
    });
    interceptor = TestBed.inject(AuthInterceptor);
  });

  it('should be created', () => {
    expect(interceptor).toBeTruthy();
  });
});
