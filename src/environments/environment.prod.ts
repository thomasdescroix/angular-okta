export const environment = {
  production: true,
  okta: {
    issuer: 'https://dev-1695350.okta.com/oauth2/default',
    clientId: '0oamxthnV2NKJK1UZ5d5',
    redirectUri: window.location.origin + '/implicit/callback'
  }
};
